This app is developed as a coding challenge for Qantas Money.

# Clean Architecture
I applied clean architecture as the overall structure of this project.  Due to the simplicity of
this app, I only used three layers (instead of four): entities, frameworkadapters, and frameworks.

# MVP
I used MVP design pattern to show data on UI.  The presenter is in the frameworkadapters layer.
The view interface is in the frameworkadapters layer, and its implementation is on the
frameworks layer.

# JSON parsing
I used Gson to do the json parsing.  The mapping data classes in the frameworkadapters layer are
generated using http://www.jsonschema2pojo.org/

# Data retrieving
I used a background-threaded intent service to retrieve the recipe json from online.  okhttp is
used to download the json.

# Local data substitution
I also implemented a local substitution of the recipe json, so that the app can be unit-tested
locally.

# Broadcasting
The recipe retrieved from online (or locally) is broadcast to the consumer running on the main
thread.


