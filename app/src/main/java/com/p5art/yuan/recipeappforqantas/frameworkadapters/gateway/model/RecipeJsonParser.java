package com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.p5art.yuan.recipeappforqantas.entities.Recipes;
import com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway.model.JsonRecipe;
import com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway.model.JsonRecipeList;

import java.util.List;

/**
 * parse recipe json string into data structure and entity objects
 */
public class RecipeJsonParser {
    public Recipes parseRecipeJsonToRecipes(String jsonString){
        return convertJsonRecipeListToRecipes(parseRecipeJsonToRecipeList(jsonString));
    }

    private JsonRecipeList parseRecipeJsonToRecipeList(String jsonString){
        Gson gson = new GsonBuilder().create();
        JsonRecipeList recipeList = gson.fromJson(jsonString, JsonRecipeList.class);
        return recipeList;
    }

    private Recipes convertJsonRecipeListToRecipes(JsonRecipeList jsonRecipeList){
        Recipes recipes = new Recipes();

        List<JsonRecipe> recipeList = jsonRecipeList.getRecipes();
        for (JsonRecipe jsonRecipe : recipeList) {
            Recipes.Recipe recipe = recipes.new Recipe(jsonRecipe.getTitle(), jsonRecipe.getHref(), jsonRecipe.getIngredients(), jsonRecipe.getThumbnail());
            recipes.addRecipe(recipe);
        }

        return recipes;
    }
}
