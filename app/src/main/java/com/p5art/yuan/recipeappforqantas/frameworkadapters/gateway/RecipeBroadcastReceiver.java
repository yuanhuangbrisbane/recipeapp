package com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.p5art.yuan.recipeappforqantas.frameworks.app.RecipeApp;

/**
 * receives recipe broadcast and pass the data to a handler
 */
public class RecipeBroadcastReceiver {
    public static final String TOKEN = "RecipeBroadcastReceiver";

    /**
     * the object that handles the recipe data
     */
    public interface RecipeBroadcastHandler{
        void handleRecipeJson(String recipeJson);
    }

    private RecipeBroadcastHandler recipeBroadcastHandler;

    public RecipeBroadcastReceiver(RecipeBroadcastHandler recipeBroadcastHandler){
        this.recipeBroadcastHandler = recipeBroadcastHandler;
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent ) {
            String recipeJson = intent.getStringExtra(TOKEN);

            if(recipeBroadcastHandler != null){
                recipeBroadcastHandler.handleRecipeJson(recipeJson);
            }
        }
    };

    public void registerBroadcastReceiver(){
        LocalBroadcastManager.getInstance(RecipeApp.instance()).registerReceiver(
                broadcastReceiver, new IntentFilter(TOKEN));
    }

    public void unregisterBroadcastReceiver(){
        LocalBroadcastManager.getInstance(RecipeApp.instance()).unregisterReceiver(
                broadcastReceiver);
    }




}
