package com.p5art.yuan.recipeappforqantas.frameworks.resource;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.p5art.yuan.recipeappforqantas.R;
import com.p5art.yuan.recipeappforqantas.commons.StreamUtils;
import com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway.RecipeJsonBroadcaster;
import com.p5art.yuan.recipeappforqantas.frameworks.app.RecipeApp;

import java.io.IOException;
import java.io.InputStream;

/**
 *  A recipe json data source which gets data from local resources
 */
public class LocalRecipeJsonBroadcaster implements RecipeJsonBroadcaster {
    private static final String TAG = "LocalRecipeJsonLoader";

    private String loadJSONFromAsset() {
        String json = null;

        try {
            InputStream inputStream = RecipeApp.instance().getResources().openRawResource(R.raw.recipe);
            json = StreamUtils.readStringFromStream(inputStream);
        } catch (IOException ex) {
            Log.e(TAG, "loadJSONFromAsset() failed.", ex);
        }

        return json;
    }

    @Override
    public void broadcastRecipeJson(String broadcastToken) {
        String recipeJson = loadJSONFromAsset();

        Intent broadcastIntent = new Intent(broadcastToken);
        broadcastIntent.putExtra(broadcastToken, recipeJson);
        LocalBroadcastManager.getInstance(RecipeApp.instance()).sendBroadcast(broadcastIntent);
    }
}
