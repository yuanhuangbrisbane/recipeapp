package com.p5art.yuan.recipeappforqantas.frameworkadapters;

import com.p5art.yuan.recipeappforqantas.entities.Recipes;
import com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway.RecipeBroadcastReceiver;
import com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway.RecipeJsonBroadcaster;
import com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway.model.RecipeJsonParser;
import com.p5art.yuan.recipeappforqantas.frameworkadapters.presenter.RecipePresenter;
import com.p5art.yuan.recipeappforqantas.frameworkadapters.presenter.RecipeView;

import java.lang.ref.WeakReference;

/**
 * Manages recipe data source and UI presentation
 */
public class RecipeManager implements RecipePresenter, RecipeBroadcastReceiver.RecipeBroadcastHandler {
    /**
     * data source of recipe
     */
    private RecipeJsonBroadcaster recipeJsonBroadcaster;

    /**
     * receiver of recipe data
     */
    private RecipeBroadcastReceiver recipeBroadcastReceiver;

    /**
     * presentation of recipe
     */
    private WeakReference<RecipeView> recipeViewWeakRef;

    public RecipeManager(RecipeJsonBroadcaster recipeJsonBroadcaster, RecipeView recipeView){
        this.recipeJsonBroadcaster = recipeJsonBroadcaster;
        this.recipeViewWeakRef = new WeakReference<>(recipeView);

        recipeBroadcastReceiver = new RecipeBroadcastReceiver(this);
        recipeBroadcastReceiver.registerBroadcastReceiver();
    }

    private RecipeView getRecipesViewFromWeakRef(){
        if(recipeViewWeakRef != null){
            return recipeViewWeakRef.get();
        }

        return null;
    }

    @Override
    public void loadRecipes() {
        if(recipeJsonBroadcaster != null){
            recipeJsonBroadcaster.broadcastRecipeJson(RecipeBroadcastReceiver.TOKEN);
        }
    }

    @Override
    public void openRecipeDetails(Recipes.Recipe recipe) {
        RecipeView recipeView = getRecipesViewFromWeakRef();

        if(recipeView != null){
            recipeView.showRecipeDetails(recipe);
        }
    }

    @Override
    public void openRecipeLink(String url) {
        RecipeView recipeView = getRecipesViewFromWeakRef();

        if(recipeView != null){
            recipeView.showRecipeWebPage(url);
        }
    }

    @Override
    public void handleRecipeJson(String recipeJson) {
        RecipeView recipeView = getRecipesViewFromWeakRef();

        if(recipeView != null){
            RecipeJsonParser recipeJsonParser = new RecipeJsonParser();
            Recipes recipes = recipeJsonParser.parseRecipeJsonToRecipes(recipeJson);
            recipeView.showRecipes(recipes);
        }
    }
}
