package com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * data structure that maps to the root JSON node of recipe JSON.
 */
public class JsonRecipeList {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("version")
    @Expose
    private Double version;
    @SerializedName("href")
    @Expose
    private String href;
    @SerializedName("results")
    @Expose
    private List<JsonRecipe> results = null;

    public String getTitle() {
        return title;
    }

    public Double getVersion() {
        return version;
    }

    public String getHref() {
        return href;
    }

    public List<JsonRecipe> getRecipes() {
        return results;
    }
}
