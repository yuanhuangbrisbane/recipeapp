package com.p5art.yuan.recipeappforqantas.frameworks.app;

import android.app.Application;

import com.p5art.yuan.recipeappforqantas.frameworkadapters.presenter.RecipePresenter;

/**
 * The application object of this app
 */
public class RecipeApp extends Application {
    private static RecipeApp application = null;

    /**
     * the instance of this app; also the application context
     */
    public static RecipeApp instance() {
        return application;
    }

    /**
     * presenter of recipes
     */
    public RecipePresenter recipePresenter = null;

    @Override
    public void onCreate() {
        super.onCreate();

        application = this;
    }



}
