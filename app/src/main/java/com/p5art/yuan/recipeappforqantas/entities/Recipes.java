package com.p5art.yuan.recipeappforqantas.entities;

import com.p5art.yuan.recipeappforqantas.commons.StringUtils;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Recipes, which contain two sets of Recipe objects: the popular ones and the other ones
 */
public class Recipes implements Serializable{

    /**
     * One recipe.
     */
    public class Recipe implements Serializable{
        String title;
        String href;
        String ingredients;
        String thumbnail;

        public Recipe(String title, String href, String ingredients, String thumbnail){
            this.title = title.trim();
            this.href = href.trim();
            this.ingredients = ingredients.trim();
            this.thumbnail = thumbnail.trim();
        }

        public String getTitle() {
            return title;
        }

        public String getHref() {
            return href;
        }

        public String getIngredients() {
            return ingredients;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj instanceof Recipe){
                Recipe recipe = (Recipe) obj;
                return recipe.title.equals(title) && recipe.href.equals(href);
            }

            return false;
        }
    }

    Set<Recipe> popularRecipeSet = new HashSet<>();
    Set<Recipe> otherRecipeSet = new HashSet<>();

    public void addRecipe(Recipe recipe){
        if(StringUtils.isNullOrEmpty(recipe.getThumbnail())){
            otherRecipeSet.add(recipe);
        } else {
            popularRecipeSet.add(recipe);
        }
    }

    public Recipe[] getPopularRecipesInArray(){
        return popularRecipeSet.toArray(new Recipes.Recipe[]{});
    }

    public Recipe[] getOtherRecipesInArray(){
        return otherRecipeSet.toArray(new Recipes.Recipe[]{});
    }

}
