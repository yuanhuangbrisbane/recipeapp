package com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway;

/**
 * interface for the data sources of the recipe json
 */
public interface RecipeJsonBroadcaster {
    /**
     * broadcast a recipe json using the broadcast token provided.
     */
    void broadcastRecipeJson(String broadcastToken);
}
