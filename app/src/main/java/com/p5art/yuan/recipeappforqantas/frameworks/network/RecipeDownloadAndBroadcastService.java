package com.p5art.yuan.recipeappforqantas.frameworks.network;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

/**
 * background thread service to download and broadcast recipe json
 */
public class RecipeDownloadAndBroadcastService extends IntentService {
    private static final String TAG = "RecipeDownloadAndBroadcastService";
    private static final String RECIPE_JSON_URL = "https://g525204.github.io/recipes.json";

    public RecipeDownloadAndBroadcastService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RecipeDownloader recipeDownloader = new RecipeDownloader(RECIPE_JSON_URL);
        String recipeJsonString = recipeDownloader.downloadRecipeJsonString();

        String broadcastToken = intent.getStringExtra(RecipeDownloadAndBroadcastService.class.getSimpleName());
        Intent broadcastIntent = new Intent(broadcastToken);
        broadcastIntent.putExtra(broadcastToken, recipeJsonString);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }


}
