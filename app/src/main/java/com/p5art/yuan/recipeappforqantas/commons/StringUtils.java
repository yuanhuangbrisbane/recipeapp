package com.p5art.yuan.recipeappforqantas.commons;

/**
 * utilities that handles string.
 */
public class StringUtils {
    public static boolean isNullOrEmpty(String string){
        if(string == null){
            return true;
        }else if(string.trim().isEmpty()){
            return true;
        }else{
            return false;
        }
    }
}
