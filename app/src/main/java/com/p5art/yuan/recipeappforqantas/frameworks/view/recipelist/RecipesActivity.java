package com.p5art.yuan.recipeappforqantas.frameworks.view.recipelist;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.p5art.yuan.recipeappforqantas.R;
import com.p5art.yuan.recipeappforqantas.entities.Recipes;
import com.p5art.yuan.recipeappforqantas.frameworkadapters.RecipeManager;
import com.p5art.yuan.recipeappforqantas.frameworks.app.RecipeApp;
import com.p5art.yuan.recipeappforqantas.frameworks.network.OnlineRecipeJsonBroadcaster;
import com.p5art.yuan.recipeappforqantas.frameworks.view.RecipeBaseActivity;

/**
 * activity that presents a list of popular and other recipes
 */
public class RecipesActivity extends RecipeBaseActivity implements RecipeItemEventHandler {
    private RecyclerView rcyPopularRecipes;
    private RecyclerView rcyOtherRecipes;

    private PopularRecipesAdapter popularRecipesAdapter;
    private OtherRecipesAdapter otherRecipesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_list);

        rcyPopularRecipes = findViewById(R.id.rcyPopularRecipes);
        rcyOtherRecipes = findViewById(R.id.rcyOtherRecipes);

        // use local json
        // RecipeApp.instance().recipePresenter = new RecipeManager(new LocalRecipeJsonBroadcaster(), this);

        // use json on the internet
        RecipeApp.instance().recipePresenter = new RecipeManager(new OnlineRecipeJsonBroadcaster(), this);

        RecipeApp.instance().recipePresenter.loadRecipes();
    }

    @Override
    protected void onResume() {
        super.onResume();

        int columnsOfTiles = getResources().getInteger(R.integer.columnsOfTiles);
        RecyclerView.LayoutManager gridLayoutManager = new GridLayoutManager(this, columnsOfTiles);
        rcyPopularRecipes.setLayoutManager(gridLayoutManager);

        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rcyOtherRecipes.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onRecipeClicked(Recipes.Recipe recipe) {
        RecipeApp.instance().recipePresenter.openRecipeDetails(recipe);
    }

    @Override
    public void showRecipes(Recipes recipes) {
        popularRecipesAdapter = new PopularRecipesAdapter(recipes.getPopularRecipesInArray(), this);
        rcyPopularRecipes.setAdapter(popularRecipesAdapter);

        otherRecipesAdapter = new OtherRecipesAdapter(recipes.getOtherRecipesInArray(), this);
        rcyOtherRecipes.setAdapter(otherRecipesAdapter);
    }
}
