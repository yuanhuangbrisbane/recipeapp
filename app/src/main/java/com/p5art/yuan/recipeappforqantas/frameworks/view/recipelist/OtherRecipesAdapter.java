package com.p5art.yuan.recipeappforqantas.frameworks.view.recipelist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.p5art.yuan.recipeappforqantas.R;
import com.p5art.yuan.recipeappforqantas.entities.Recipes;
import com.p5art.yuan.recipeappforqantas.frameworks.view.recipelist.OtherRecipesAdapter.RecipeListItem;

/**
 * RecyclerView adapter that provides views of the not-so-popular recipes
 */
public class OtherRecipesAdapter extends RecyclerView.Adapter<RecipeListItem> {

    class RecipeListItem extends RecyclerView.ViewHolder {
        public TextView recipeDescription;

        public RecipeListItem(View view) {
            super(view);
            recipeDescription = view.findViewById(R.id.recipeDescription);
        }
    }

    private Recipes.Recipe[] recipes;
    private RecipeItemEventHandler recipeItemEventHandler;

    public OtherRecipesAdapter(Recipes.Recipe[] recipes, RecipeItemEventHandler recipeItemEventHandler) {
        this.recipes = recipes;
        this.recipeItemEventHandler = recipeItemEventHandler;
    }

    @Override
    public RecipeListItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_recipe, parent, false);

        return new RecipeListItem(itemView);
    }

    @Override
    public void onBindViewHolder(RecipeListItem recipeListItem, final int position) {
        final Recipes.Recipe recipe = recipes[position];

        recipeListItem.recipeDescription.setText(recipe.getTitle());

        recipeListItem.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recipeItemEventHandler.onRecipeClicked(recipe);
            }
        });
    }

    @Override
    public int getItemCount() {
        return recipes.length;
    }
}