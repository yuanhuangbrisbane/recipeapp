package com.p5art.yuan.recipeappforqantas.frameworks.view.recipelist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.p5art.yuan.recipeappforqantas.R;
import com.p5art.yuan.recipeappforqantas.entities.Recipes;
import com.p5art.yuan.recipeappforqantas.frameworks.app.RecipeApp;
import com.p5art.yuan.recipeappforqantas.frameworks.view.recipelist.PopularRecipesAdapter.*;

/**
 * RecyclerView adapter that provides views of the popular recipes
 */
public class PopularRecipesAdapter extends RecyclerView.Adapter<RecipeTile> {

    class RecipeTile extends RecyclerView.ViewHolder {
        public ImageView recipeImage;
        public TextView recipeDescription;

        public RecipeTile(View view) {
            super(view);
            recipeImage = view.findViewById(R.id.recipeImage);
            recipeDescription = view.findViewById(R.id.recipeDescription);
        }
    }

    private Recipes.Recipe[] recipes;
    private RecipeItemEventHandler recipeUIEventHandler;

    public PopularRecipesAdapter(Recipes.Recipe[]  recipes, RecipeItemEventHandler recipeItemEventHandler) {
        this.recipes = recipes;
        this.recipeUIEventHandler = recipeItemEventHandler;
    }

    @Override
    public RecipeTile onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tile_recipe_w_img, parent, false);

        return new RecipeTile(itemView);
    }

    @Override
    public void onBindViewHolder(RecipeTile recipeTile, final int position) {
        final Recipes.Recipe recipe = recipes[position];

        recipeTile.recipeDescription.setText(recipe.getTitle());

        Glide.with(RecipeApp.instance())
                .load(recipe.getThumbnail())
                .into(recipeTile.recipeImage);

        recipeTile.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recipeUIEventHandler.onRecipeClicked(recipe);
            }
        });
    }

    @Override
    public int getItemCount() {
        return recipes.length;
    }




}