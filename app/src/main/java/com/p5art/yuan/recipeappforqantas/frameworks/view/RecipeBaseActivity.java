package com.p5art.yuan.recipeappforqantas.frameworks.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.p5art.yuan.recipeappforqantas.entities.Recipes;
import com.p5art.yuan.recipeappforqantas.frameworkadapters.presenter.RecipeView;
import com.p5art.yuan.recipeappforqantas.frameworks.view.recipedetails.RecipeDetailsActivity;
import com.p5art.yuan.recipeappforqantas.frameworks.view.recipelist.RecipesActivity;
import com.p5art.yuan.recipeappforqantas.frameworks.view.webview.WebViewActivity;

/**
 * Base activity of recipe activities.  It also arranges activity switching between recipe activities.
 */
public class RecipeBaseActivity extends AppCompatActivity implements RecipeView {
    @Override
    public void showRecipes(Recipes recipes) {
        Intent intent = new Intent(this, RecipesActivity.class);
        startActivity(intent);
    }

    @Override
    public void showRecipeDetails(Recipes.Recipe recipe) {
        Intent intent = new Intent(this, RecipeDetailsActivity.class);
        intent.putExtra(RecipeDetailsActivity.class.getSimpleName(), recipe);
        startActivity(intent);
    }

    @Override
    public void showRecipeWebPage(String url) {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(WebViewActivity.class.getSimpleName(), url);
        startActivity(intent);
    }
}
