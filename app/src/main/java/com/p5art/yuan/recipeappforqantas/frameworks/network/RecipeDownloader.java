package com.p5art.yuan.recipeappforqantas.frameworks.network;

import android.util.Log;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * http client for downloading recipe json
 */
public class RecipeDownloader {
    private static final String TAG = "RecipeDownloader";

    private OkHttpClient client = new OkHttpClient();
    private final String url;

    public RecipeDownloader(String url) {
        this.url = url;
    }

    public String downloadRecipeJsonString() {
        String retVal = null;

        Request request = new Request.Builder()
                .url(url)
                .build();

        try {
            Response response = client.newCall(request).execute();
            retVal = response.body().string();
        } catch (IOException e) {
            Log.e(TAG, "downloadRecipeJsonString() failed.", e);
        }

        return retVal;
    }
}
