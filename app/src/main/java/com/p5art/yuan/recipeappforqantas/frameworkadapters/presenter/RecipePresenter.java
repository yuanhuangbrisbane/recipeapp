package com.p5art.yuan.recipeappforqantas.frameworkadapters.presenter;

import com.p5art.yuan.recipeappforqantas.entities.Recipes;

/**
 * Interface for the View in the MVP pattern to talk to the Presenter
 */
public interface RecipePresenter {
    /**
     * instruct Presenter to load recipes
     */
    void loadRecipes();

    /**
     *  open recipe details
     */
    void openRecipeDetails(Recipes.Recipe recipe);

    /**
     * open recipe web page
     */
    void openRecipeLink(String url);
}
