package com.p5art.yuan.recipeappforqantas.frameworks.view.recipelist;

import com.p5art.yuan.recipeappforqantas.entities.Recipes;

/**
 * interface for the objects that can handle recipe item UI events (click etc.)
 */
public interface RecipeItemEventHandler {
        void onRecipeClicked(Recipes.Recipe recipe);
}
