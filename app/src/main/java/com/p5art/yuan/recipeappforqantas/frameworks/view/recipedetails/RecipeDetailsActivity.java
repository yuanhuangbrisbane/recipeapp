package com.p5art.yuan.recipeappforqantas.frameworks.view.recipedetails;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.p5art.yuan.recipeappforqantas.R;
import com.p5art.yuan.recipeappforqantas.entities.Recipes;
import com.p5art.yuan.recipeappforqantas.frameworks.app.RecipeApp;
import com.p5art.yuan.recipeappforqantas.frameworks.view.RecipeBaseActivity;

/**
 * activity that presents one recipe's details
 */
public class RecipeDetailsActivity extends RecipeBaseActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_details);

        Bundle extras = getIntent().getExtras();
        final Recipes.Recipe recipe = (Recipes.Recipe) extras.get(RecipeDetailsActivity.class.getSimpleName());

        TextView txvRecipeName = findViewById(R.id.txvRecipeName);
        txvRecipeName.setText(recipe.getTitle());

        TextView txvIngredients = findViewById(R.id.txvIngredients);
        txvIngredients.setText(recipe.getIngredients());

        TextView txvRecipeLink = findViewById(R.id.txvRecipeLink);
        txvRecipeLink.setText(recipe.getHref());

        ImageView imgRecipePhoto = findViewById(R.id.imgRecipePhoto);
        Glide.with(RecipeApp.instance())
                .load(recipe.getThumbnail())
                .into(imgRecipePhoto);

        txvRecipeLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RecipeApp.instance().recipePresenter.openRecipeLink(recipe.getHref());
            }
        });
    }
}
