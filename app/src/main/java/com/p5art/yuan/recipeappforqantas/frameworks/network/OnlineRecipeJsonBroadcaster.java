package com.p5art.yuan.recipeappforqantas.frameworks.network;

import android.content.Context;
import android.content.Intent;

import com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway.RecipeJsonBroadcaster;
import com.p5art.yuan.recipeappforqantas.frameworks.app.RecipeApp;

/**
 *  A recipe json data source which fetches data from online
 */
public class OnlineRecipeJsonBroadcaster implements RecipeJsonBroadcaster {
    @Override
    public void broadcastRecipeJson(String broadcastToken) {
        Context appContext = RecipeApp.instance();
        Intent intent = new Intent(appContext, RecipeDownloadAndBroadcastService.class);
        intent.putExtra(RecipeDownloadAndBroadcastService.class.getSimpleName(), broadcastToken);
        appContext.startService(intent);
    }
}
