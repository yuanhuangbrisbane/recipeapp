package com.p5art.yuan.recipeappforqantas.frameworkadapters.presenter;

import com.p5art.yuan.recipeappforqantas.entities.Recipes;

/**
 * interface for the views in the MVP pattern.
 */
public interface RecipeView {
    /**
     * show recipes as a list
     */
    void showRecipes(Recipes recipes);

    /**
     * show the details of one recipe
     */
    void showRecipeDetails(Recipes.Recipe recipe);

    /**
     * show web page of recipe
     */
    void showRecipeWebPage(String url);
}
