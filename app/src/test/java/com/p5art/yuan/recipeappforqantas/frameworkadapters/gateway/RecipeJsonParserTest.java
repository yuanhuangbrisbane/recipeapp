package com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway;

import com.p5art.yuan.recipeappforqantas.commons.StreamUtils;
import com.p5art.yuan.recipeappforqantas.entities.Recipes;
import com.p5art.yuan.recipeappforqantas.frameworkadapters.gateway.model.RecipeJsonParser;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * Unit test of the recipe JSON parser
 */
public class RecipeJsonParserTest {

    @Test
    public void parseRecipeJsonAndConvertToRecipesTest() throws IOException {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("recipe.json");
        assertNotNull(inputStream);
        String recipeJsonString = StreamUtils.readStringFromStream(inputStream);
        RecipeJsonParser recipeJsonParser = new RecipeJsonParser();
        Recipes recipes = recipeJsonParser.parseRecipeJsonToRecipes(recipeJsonString);
        Recipes.Recipe[] popularRecipesInArray = recipes.getPopularRecipesInArray();
        assertEquals(15, popularRecipesInArray.length);
        Recipes.Recipe[] otherRecipesInArray = recipes.getOtherRecipesInArray();
        assertEquals(13, otherRecipesInArray.length);
    }
}